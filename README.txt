HDFView 3.3.0
------------------------------------------------------------------------------

This directory contains the binary (release) distribution of
HDFView 3.3.0 that was compiled on:
      Linux 5.13.0-1031-aws

with:  Java JDK 17.0.3

It was built with the following options:
   -- SHARED HDF 4.2.16
   -- SHARED HDF5 1.14.0

===========================================================================
Note: By default HDFView runs on the included Java JRE 17.
===========================================================================

The contents of this directory are:

   COPYING                 - Copyright notice
   README.txt              - This file
   hdfview-3.3.0-Linux.rpm - HDFView Installer

Installation
===========================================================================
To install HDFView for Linux
    Install RPM File Using RPM Command
    ------------------------------------------------------------------------------
             To install a .rpm package in CentOS Linux, enter the following:
        sudo rpm -i hdfview-3.3.0-1.x86_64.rpm
             The -i switch tells the package manager you want to install the file.

    More information on the RPM installer can be found in the RPM documentation.
             ------------------------------------------------------------------------------


1. Install RPM File with dnf
             ------------------------------------------------------------------------------
             Alternately, you can use the dnf utility to install .rpm files.

             Enter the following:
                 sudo dnf localinstall hdfview-3.3.0-1.x86_64.rpm
             The localinstall option instructions dnf to look at your current working directory for the installation


    Remove RPM Package
             ------------------------------------------------------------------------------
             The RPM installer can be used to remove (or uninstall) a software package.

             Enter the following into a terminal window:
             sudo rpm -e HDFView-3.3.0-1.x86_64
             The -e option instructs RPM to erase the software.


===========================================================================

The executable will be in the installation location,
    which by default is at /opt/hdfview/bin/HDFView

The general directory layout for each of the supported platforms follows:
===========================================================================
Linux
===========================================================================
HDFView/
  bin/            // Application launchers
    HDFView
  lib/
    app/
      doc/        // HDFView documents
      extra/      // logging jar for simple logs
      mods/       // Application duplicates
      samples/    // HDFView sample files
      HDFView.cfg     // Configuration info, created by jpackage
      HDFView.jar     // JAR file, copied from the --input directory
    runtime/      // Java runtime image
===========================================================================
macOS
===========================================================================
HDFView.app/
  Contents/
    Info.plist
    MacOS/         // Application launchers
      HDFView
    Resources/           // Icons, etc.
    app/
      doc/        // HDFView documents
      extra/      // logging jar for simple logs
      mods/       // Application duplicates
      samples/    // HDFView sample files
      HDFView.cfg     // Configuration info, created by jpackage
      HDFView.jar     // JAR file, copied from the --input directory
    runtime/      // Java runtime image
===========================================================================
Windows
===========================================================================
HelloWorld/
  HDFView.exe       // Application launchers
  app/
    doc/        // HDFView documents
    extra/      // logging jar for simple logs
    mods/       // Application duplicates
    samples/    // HDFView sample files
    HDFView.cfg     // Configuration info, created by jpackage
    HDFView.jar     // JAR file, copied from the --input directory
  runtime/      // Java runtime image
===========================================================================

Documentation for this release can be found at the following URL:
   https://portal.hdfgroup.org/display/HDFVIEW/HDFView

See the HDF-JAVA home page for further details:
   https://www.hdfgroup.org/downloads/hdfview/

Bugs should be reported to help@hdfgroup.org.